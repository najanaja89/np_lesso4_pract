﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace np_lesso4_pract
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket srvSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            srvSocket.Bind(new IPEndPoint(IPAddress.Any, 12345));
            EndPoint clientEndPoint = new IPEndPoint(0, 0);
            byte[] buf = new byte[64 * 1024];

            while (true)
            {
                int recSize = srvSocket.ReceiveFrom(buf, ref clientEndPoint);

                //Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                int sendSize = srvSocket.SendTo(Encoding.UTF8.GetBytes(DateTime.Now.TimeOfDay.ToString()), clientEndPoint); 
            }

        }
    }
}
