﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace np_lesso4_pract_client
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            var str = "Hello ";
            string ipSrv = "127.0.0.1";
            int port = 12345;
            EndPoint srvEndPoint = new IPEndPoint(0, 0);
            int sendSize = client.SendTo(Encoding.UTF8.GetBytes(str), srvEndPoint = new IPEndPoint(IPAddress.Parse(ipSrv), port));

            //Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //clientSocket.Bind(new IPEndPoint(IPAddress.Any, 12345));
            byte[] buf = new byte[64 * 1024];
           
            int recSize = client.ReceiveFrom(buf, ref srvEndPoint);
            textBoxCurrentTime.Text = Encoding.UTF8.GetString(buf, 0, recSize);

        }
    }
}
